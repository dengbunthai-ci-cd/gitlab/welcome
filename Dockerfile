FROM openjdk:8-jdk-alpine
MAINTAINER dengbunthai@gmail.com

RUN mkdir target
COPY target target
WORKDIR target
ENTRYPOINT ["java", "-jar", "welcome-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080