package com.dengbunthai.group.welcome.api;

import com.dengbunthai.group.welcome.service.LogicCalculationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/api")
public class HelloRestController {

    @Autowired
    LogicCalculationService service;

    private Logger logger = LoggerFactory.getLogger(HelloRestController.class);

    @GetMapping("hello")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("Hello world!");
    }

    @GetMapping("sum")
    public ResponseEntity<Object> sum(
        @RequestParam(value = "nums", required = false) String _num
    ) {

        int[] nums = {1, 2, 3, 4, 5};
        if (_num != null && !_num.isEmpty()) {
            nums = Arrays.stream(_num.split(",")).mapToInt(value -> Integer.parseInt(value)).toArray();
        }
        int total = service.sum(nums);
        logger.info("Access some value sum: [{}]", total);
        return ResponseEntity.status(HttpStatus.OK).body(
            new HashMap<String, Object>() {{
                put("total2", total);
            }}
        );
    }

    @GetMapping("subtract")
    public ResponseEntity<Object> subtract(
        @RequestParam(value = "nums", required = false) String _num
    ) {

        int[] nums = {1, 2, 3, 4, 5};
        if (_num != null && !_num.isEmpty()) {
            nums = Arrays.stream(_num.split(",")).mapToInt(value -> Integer.parseInt(value)).toArray();
        }
        int total = service.subtract(nums);
        logger.info("Access some value subtract: [{}]", total);
        return ResponseEntity.status(HttpStatus.OK).body(
            new HashMap<String, Object>() {{
                put("total2", total);
            }}
        );
    }


    @GetMapping("multiply")
    public ResponseEntity<Object> multiply(
        @RequestParam(value = "nums", required = false) String _num
    ) {

        int[] nums = {1, 2, 3, 4, 5};
        if (_num != null && !_num.isEmpty()) {
            nums = Arrays.stream(_num.split(",")).mapToInt(value -> Integer.parseInt(value)).toArray();
        }
        int total = service.multiply(nums);
        logger.info("Access some value multiply: [{}]", total);
        return ResponseEntity.status(HttpStatus.OK).body(
            new HashMap<String, Object>() {{
                put("total2", total);
            }}
        );
    }

    @GetMapping("divide")
    public ResponseEntity<Object> divide(@RequestParam(value = "nums", required = false) String _num) {
        int[] nums = {10, 2};
        if (_num != null && !_num.isEmpty()) {
            nums = Arrays.stream(_num.split(",")).mapToInt(value -> Integer.parseInt(value)).toArray();
        }
        int total = service.divide(nums);
        logger.info("Access some value divide: [{}]", total);
        return ResponseEntity.status(HttpStatus.OK).body(
            new HashMap<String, Object>() {{
                put("total2", total);
            }}
        );
    }

}
