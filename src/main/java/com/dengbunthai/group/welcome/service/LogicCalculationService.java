package com.dengbunthai.group.welcome.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class LogicCalculationService {
    public int sum(int... nums) {
        return Arrays.stream(nums).sum();
    }

    public int subtract(int... nums) {
        return Arrays.stream(nums).reduce((left, right) -> left - right).getAsInt();
    }

    public int multiply(int... nums) {
        return Arrays.stream(nums).reduce((left, right) -> left * right).getAsInt();
    }

    public int divide(int... nums) {
        return Arrays.stream(nums).reduce((left, right) -> left / right).getAsInt();
    }

}
